#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

using namespace std;
void output(ofstream&, string, bool, bool, bool, string, string);
void header(ofstream&);

int main () { 
   //variables
    string user="MCBAINAC69";
    bool compile = true;
    bool memleak = true;
    bool o = true;
    string input = "Input";
    string out = "Output";
    ofstream file;
    header(file);
    
   //function call
    output(file, user, compile, memleak, o, input, out);
    user = "test";
    output(file, user, compile, false, o, input, out);

    user="testit4321";
    output(file, user, false, memleak, false, input, out);
    
    user="lastfi5678";
    output(file, user, false, false, false, input, out);

    return 0; 
    } 

void output(ofstream& file, string user, bool compile, bool memleak, bool o, string input, string out) {
    int length = user.length();
    if (length < 10) {
      user.append(10 - length, ' ');
    }
    
    file.open ("Results.txt", fstream::app);
    
    char com;
    char mem;
    char oo;
    
    if(compile) {
      com = 'T';
    } else {
      com = 'F';
    }

    if(memleak) {
      mem = 'Y';
    } else {
      mem = 'N';
    }

    if(o) {
      oo = 'T';
    } else {
      oo = 'F';
    }

    file << "|  " << user << setw(3) << "|" << setw(8) << com << setw(8) << "|" << setw(8) << mem << setw(8) << "|" << setw(10) << oo << setw(10) << "|";
    //file << user << "\t" << compile << "\t" << memleak << "\n";
     //file << "\tOutput Differences: " << input << " || " << out << endl <<endl;
     if(!o){
       file << " --->  Actual: " << input << " | Expected: " << out <<" | -- ";
     }
     file << endl;
    file.close();

}
//open file and put a header in it
void header(ofstream& file){
    file.open ("Results.txt");

    file << "| Student Name " << "| Compiled(T/F) " << "| Memleaks(Y/N) " << "| Output Match(T/F) |" << endl;

    file.close();
}