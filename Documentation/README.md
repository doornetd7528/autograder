# Autograder
*Note: Project and README are subject to change.*

This is an autograding program for teachers grading assignments written in C. The program is written in C++ and currently prompts the users for the relative path to a zip file. That zip file archive is expected to hold a series of other zip files, each containing a c program made by a student. The Autograder prompts for the following contingencies:

    * Is an input file needed to run student programs? (Y/N)
        * If so, enter relative pathname to a text (.txt) file: 
    * Is a makefile required for this assignment? (Y/N)
    * Is there a console output that we need to compare against the students' outputs?
        * If so, enter relative pathname to a text (.txt) file:

Once the program finishes grading assignments a single file will be created showing each student's name, students' individual successes and failures, and general group statistics. 

| Student Names | Compiled (T/F)| Output Match (T/F) |
| ------------- |:-------------:| -----:|
| Alice         | T             | T     |
| John          | F             | F     |
| Sue           | F             | F     |
| Bob           | T             | T     |

*Note: the above table is to represent a possible format that the stat file will take but is not a promised result by any means. *

## Getting Started

1. To compile v0.0 code use: `g++ -o main unzip.cc -lzip` .
2. To run v0.0 application use: `./main`
3. Follow prompts properly and the application will grade the assignments.


### Installing

* g++:
    * You will need to install the c++ compiler g++ to run this project. 
    * Command: `sudo apt install g++`

* libzip:
    * You will need to install the c library called libzip and compile using the appended tag [-lzip].
    * Command: `apt-get install libzip-dev`

## Running the tests

Not Applicable yet.

## Versioning

Current: Version 0.0 of Autograder

## Authors

* **Hannah Borreson** 
* **Tim Doornek** 
* **Add Your Name Here** 
* **Add Your Name Here** 
* **Add Your Name Here** 
* **Add Your Name Here** 
* **Add Your Name Here** 

## License

Product is property of students mentioned in aforementioned author section and also owned by client Dr. Gurung of the University of Wisconsin-Eau Claire.

## Acknowledgments

* coffee

