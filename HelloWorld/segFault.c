/*
	This is a sample c program that compiles successfully but seg faults.
*/

#include <stdio.h>

int main() { 
	
	printf("we out here\n");
	
	printf("we still out here\n");

	char *str;  
  
	
	
	/* Stored in read only part of data segment */
	//str = "GfG";      
  
	/* Problem:  trying to modify read only memory */
	//*(str+1) = 'n';  
	return 0; 
} 
