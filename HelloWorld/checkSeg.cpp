/*
	This is a sample c program that parses a program's output into a string vector.
	It also checks to see if the program seg faults.
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "pstream.h"

using namespace std;


//thePath: file path to the executable program
//outputStrings: a reference to a vector of strings that stores the terminal output of the process
bool runProcess(string thePath, vector<string>& outputStrings){
	bool segFault = false;
	
	// run the process and create a streambuf that reads its stdout and stderr
	//the first parameter is the name of the executable
	redi::ipstream proc(thePath, redi::pstreams::pstdout | redi::pstreams::pstderr);
	string line;
	
	// read the output of the process
	while (getline(proc.out(), line)){
		outputStrings.push_back(line);
		//cout << "stdout: " << line << '\n';
	}
	
	//get exit code of process
	proc.close();
	if(proc.rdbuf()->exited()){
		int result = proc.rdbuf()->status();
		if (result == 11) {
			segFault = true;
		}
	}
	
	return segFault;
}

int main() {
	
	vector<string> outputStrings;
	bool fault = runProcess("./segFault", outputStrings);
	
	cout << "fault: " << fault << endl;
	
	//loop through the output vector and print
	for(vector<string>::const_iterator i = outputStrings.begin(); i != outputStrings.end(); ++i) {
		cout << *i << "\n"; // this will print all the contents of *outputStrings*
	}
}
