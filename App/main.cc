#include <iostream>
#include "zip.h"
#include <string.h>
#include <stdio.h>
#include <dirent.h>
#include <bits/stdc++.h>
#include "output.cc"
using namespace std;

std::string getFileExtension(std::string filePath)
{
    std::string extension;
	int size = filePath.length();
    int i = 0;
    for (i = 0; i < size - 1; i++){
        if(filePath.at(i) == '.'){
            extension = filePath.substr (i+1, size-1);
        }
    }
    return extension;
}

int main()
{
    //variables 
    char inputYN;
    char inputYN2;
    char inputYN3;
    bool useMake = false;
    string zipPath;
    string inputPath;
    string outputPath;
    string directory;
    
    //input prompting
    cout << cyanText << "\nEnter relative path to directory with zipfiles: " << normalText;
    cin >> zipPath;
    cout << cyanText << "\nIs an input file needed to run the students program? Y / N: " << normalText;
    cin >> inputYN;
        if (inputYN == 'Y' || inputYN =='y' ){
            cout << "Enter relative path to input file: ";
            cin >> inputPath;
        }
    
    cout << cyanText << "\nWill the students have a console output that will need to be compared to an expected output? Y / N: " << normalText;
    cin >> inputYN2;
        if (inputYN2 == 'Y'|| inputYN2 == 'y') {
            cout << cyanText << "Enter relative path to output file: " << normalText;
            cin >> outputPath;
        }

    cout << cyanText << "\nAre the students expected to have a Makefile? Y / N: " << normalText; 
    cin >> inputYN3;
        if(inputYN3 == 'Y'|| inputYN3 == 'y'){
            useMake = true;
        }

    cout << greenText <<  "\n\n\nRunning Autograder... \n\n" << normalText; 

    //open file for results output
    ofstream outputFile("Results.txt",ofstream::app);

    //unzipping variables
    struct dirent *entry;
    DIR *dp;
    char *path = new char[zipPath.length() + 1];
    strcpy(path, zipPath.c_str());
    string studentName;

    dp = opendir(path);
    if (dp != 0) {
        
	// Check every file in directory
        while ((entry = readdir(dp))){
            // Open the ZIP archive
            if(getFileExtension(entry->d_name) == "zip"){
             	int err = 0;
             	std::string directoryIndicator = "/";
             	std::string fileName = path + directoryIndicator + entry->d_name;
		cout << greenText << "\tUnzipping file: " << normalText << entry->d_name << endl;
	     	char *cFileName = new char[fileName.length() + 1];
             	strcpy(cFileName, fileName.c_str());
             	zip *z = zip_open(cFileName, 0, &err);

             	const int numberFiles = zip_get_num_entries(z, 0);
             	//cout << "\n\t\t\tZip File Statistics";
             	//cout << "\nNumber of files inside of this Zip File: " << numberFiles;

             	for(int i=0; i < numberFiles; i++){

             		const char *foundName = zip_get_name(z, i, 0);
			
           		studentName = foundName;
		    
                	// Search for the file of given name
                	struct zip_stat st;
                	zip_stat_init(&st);
                	const int stat = zip_stat(z, foundName, 0, &st);
			
               		// Alloc memory for its uncompressed contents
                	char *contents = new char[st.size];

                	// Read the compressed file
                	zip_file *f = zip_fopen(z, foundName, 0);
	                zip_fread(f, contents, st.size);
		   
			//write unzipped file to disk
			string fullPath = path + directoryIndicator + foundName; 
			if(!std::ofstream(foundName).write(contents, st.size)){
				std::cerr << "\tError writing file" << '\n';
				return EXIT_FAILURE;
			}
			 
                	/** Calls to checks can happen here. **/
                	//cout << "\nName of File at Index " << i << " : " << foundName;
                	//cout << "\nSize: " << st.size;
                	//cout << "\nStat Return : " << stat;
                	//cout << "\nContents : " << contents;
                	//cout << "\n\n";
                    
			//send file to middleman	
			output(outputFile, foundName, useMake, "");			
			remove(foundName);

			delete(contents); 
                	delete(cFileName);
                	zip_fclose(f);
             	}

             	// And close the archive
             	zip_close(z);
            }
        }
    }

    // Close directory 
    closedir(dp);
    delete(path);

    cout << greenText << "Autograder has completed execution. Please check the results.txt file in this directory for results.\n\n\n" << normalText << endl;
    return 0;
}
