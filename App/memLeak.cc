bool memLeak() {
  bool leak = true;
  system("valgrind --log-file=vg ./a.out");  

  ifstream file("vg");
  string line = "";
  while(getline(file, line)) {
    string find = "no leaks are possible";
    size_t found = line.find(find);

    if(found != string::npos) {
      leak = false;
      break;
    }
  }

  file.close();

  system("rm vg");

  return leak;
}
