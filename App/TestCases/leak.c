#include <stdlib.h>
#include <stdio.h>

int main()
{
    char *x = malloc(100); /* or, in C++, "char *x = new char[100] */
    printf("This is a test file that should have a leak\n");
    return 0;
}
