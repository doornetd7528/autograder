#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include "compiler.cc"
#include "memLeak.cc"

using namespace std;
//void output (ofstream&, string, bool,string); //Takes in a ofstream, a filepath, whether it makes, expected output

//void header(ofstream&);

// int main () { 
//    //variables
//     string dirPath = "untitled.c";
//     bool make = false;
//     string exOut = "";
   
//     ofstream file;
//     header(file);

//     output(file, dirPath, make, exOut);

//     return 0; 
//     } 

void output(ofstream& file, string dirPath, bool make ,string exOut) { //ofstream& file, string user, bool compile, bool memleak, bool o, string input, string out
    //placeholder variables to be removed later
    bool o = true;
    bool memleak = false;
    string user = dirPath;
    string input = "Input";
    string output = "Output";
    
    //file.open ("Results.txt", fstream::app);
    
    char com='F';
    char mem='N';
    char out='F';

    if(make){
      //run make compilation
      if(0) { //Place makefile function here
        com = 'T';
      }
    }else{
      if(compile(dirPath)) {
        com = 'T';
      }
    }


    //check to see if the compilation works either with the makefile or the standard way
    //If it does not compile with either method skip the rest of the tests and print to the file
    if(com=='T'){
    int length = dirPath.length();
    if (length < 10) {
      user.append(10 - length, ' ');
    }
    
    if(1) { //memLeak()
      mem = 'Y';
    } 

    if(o) {
      out = 'T';
    } 
    

    file << "|  " << user << setw(3) << "|" << setw(8) << com << setw(8) << "|" << setw(8) << mem << setw(8) << "|" << setw(10) << out << setw(10) << "|";
    //file << user << "\t" << compile << "\t" << memleak << "\n";
     //file << "\tOutput Differences: " << input << " || " << out << endl <<endl;
     if(!o){
       file << " --->  Actual: " << input << " | Expected: " << out <<" | -- ";
     }
     file << endl;
    file.close();
    }
     file << "|  " << user << setw(3) << "|" << "DOES NOT COMPILE" <<endl;

}
//open file and put a header in it
void header(ofstream& file){
    file.open ("Results.txt");

    file << "| Student Name " << "| Compiled(T/F) " << "| Memleaks(Y/N) " << "| Output Match(T/F) |" << endl;

    file.close();
}
