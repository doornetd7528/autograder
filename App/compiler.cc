#include <iostream>
#include <stdlib.h>
using namespace std; 

const string normalText = "\033[0m";
const string cyanText = "\033[0;36m";
const string greenText = "\033[0;32m";
const string yellowText = "\033[0;33m";

//bool compile(char*);


// int main () 
// { 
//     char filename[100]; 
//     cout << "Enter file name to compile "; 
//     cin.getline(filename, 100); 
    
//     bool res=compile(filename);

//       cout << "Result: " << res << '\n';

//       return 0; 
  

//     } 
  
    
    bool compile(string filename){
      // Build command to execute.  For example if the input`
    // file name is a.cpp, then str holds "gcc -o a.out a.cpp"  
    // Here -o is used to specify executable file name 
    string str = "gcc "; 
    str = str + "-o a.out " + filename ;//+ ">$null";
    
    
    // Convert string to const char * as system requires 
    // parameter of type const char * 
    const char *command = str.c_str(); 

    cout << yellowText << "\tCompiling file " << normalText << filename << yellowText << " with\t " << normalText <<  command << '\n' << endl; 
    int status=system(command); 

    if (status == 0){
        //cout << "Compiled Succesfully!" << '\n';
        return true;
        //cout << "Error: " << strerror(errno) << '\n';
    }else{
        //cout << "There was an error during compilation" << '\n';
        //cout << "Error " << strerror(errno) << '\n';   //Lets you print out the error number 
        return false; 
    }
  
    //If we wanted to run the file and check its output
        // cout << "\nRunning file "; 
        //system("./a.out"); 
  

    }
